<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    protected $redirectLogout = "login";
    protected $redirectLogin = "home";

    public function showLoginForm()
    {
        return view("auth.login");
    }

    public function login(LoginRequest $request)
    {
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse();
        }

        return $this->sendFailLoginResponse();
    }

    public function logout()
    {
        Auth::logout();
        request()->session()->invalidate();
        return redirect()->route($this->redirectLogout);
    }

    public function impersonate($credentials)
    {
        // TODO transferir para o repo
        $user = User::where($this->impersonateField($credentials), $credentials)->first();
        if (!isset($user->id) || config("sysconfig.auth.impersonation") !== true) {
            return $this->sendFailImpersonateResponse();
        }

        Auth::loginUsingId($user->id);

        return $this->sendLoginResponse();
    }

    protected function attemptLogin($request)
    {
        return Auth::attempt($this->credentials($request), config("sysconfig.auth.alwaysRemeber"));
    }

    protected function credentials($request)
    {
        $credentials = [
            $this->username($request) => $request->user,
            "password" => $request->password,
            "active" => true
        ];

        return $credentials;
    }

    protected function username($request)
    {
        if (filter_var($request->user, FILTER_VALIDATE_EMAIL)
         || config("sysconfig.auth.usernameLogin") !== true) {
            return "email";
        }

        return "username";
    }

    protected function impersonateField($field)
    {
        if (filter_var($field, FILTER_VALIDATE_EMAIL)) {
            return "email";
        } elseif (intval($field)) {
            return "id";
        }

        return "username";
    }

    protected function sendLoginResponse()
    {
        // TODO definir mensagem de bem vindo
        return redirect()->intended($this->redirectLogin);
    }

    protected function sendFailLoginResponse()
    {
        throw ValidationException::withMessages([
            "user" => Lang::get("auth.failed"),
        ]);
    }

    protected function sendFailImpersonateResponse()
    {
        // TODO Exibir mensagem de Erro;
        return redirect()->route($this->redirectLogin)->withErrors(["user"=>"Usuário não encontrado"]);
    }
}
