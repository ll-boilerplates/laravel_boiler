<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    protected $table = "permissions";
    protected $fillable = [
        "name", "permission", "permission_group_id"
    ];

    public function roles()
    {
        return $this->belongsToMany("App\Models\Role", "permission_roles");
    }
}
