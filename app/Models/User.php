<?php

namespace App\Models;

use App\Notifications\ResetPasswordEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, CanResetPassword;

    protected $table = "users";
    protected $fillable = [
        "name", "email", "username", "password", "active"
    ];
    protected $hidden = [
        "password", "remember_token",
    ];
    protected $casts = [
        "email_verified_at" => "datetime",
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordEmail($token, $this));
    }

    public function roles()
    {
        return $this->belongsToMany("App\Models\Role");
    }

    public function isAdmin()
    {
        dd($this->roles->where("id", 0)->count() > 0);
    }

    public function hasPermission(Permission $permission)
    {
        return $this->hasAnyRole($permission->roles);
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles) || is_object($roles)) {
            return !! $this->roles->intersect($this->roles)->count();
        }
        return $this->roles->contains("name", $roles);
    }
}
