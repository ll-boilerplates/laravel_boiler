<?php

namespace App\Repositories;

class BaseRepository
{
    protected $modelClass;
    protected $take = 15;

    protected function newQuery()
    {
        return app($this->modelClass)->newQuery();
    }

    protected function doQuery($query = null, $take = null, $paginate = true)
    {
        if (is_null($query)) {
            $query = $this->newQuery();
        }
        if (true == $paginate) {
            return $query->paginate($take);
        }

        if ($take > 0 || false !== $take) {
            $query->take($take);
        }
        return $query->get();
    }

    public function getAll($take = 15, $paginate = true)
    {
        return $this->doQuery(null, $take, $paginate);
    }

    public function lists($column, $key = null)
    {
        return $this->newQuery()->lists($column, $key);
    }

    public function find($id, $fail = true)
    {
        if ($fail) {
            return $this->newQuery()->findOrFail($id);
        }
        return $this->newQuery()->find($id);
    }

    public function create($data)
    {
        return $this->newQuery()->create($data);
    }

    public function update($id, $data)
    {
        $model = $this->find($id);
        return $model->update($data);
    }

    public function delete($id, $fail = true)
    {
        $model = $this->find($id, $fail);

        if ($model) {
            return $model->delete();
        }

        return false;
    }

    public function findByField($field, $value, $like = true, $fail = true)
    {
        $operation = $like ? "like" : "=";
        $value = $like ? ("%" . $value . "%") : $value;

        $result = $this->newQuery()->where($field, $operation, $value)->get();

        if ($fail && count($result) <= 0) {
            abort(404);
        }

        return $result;
    }

}
