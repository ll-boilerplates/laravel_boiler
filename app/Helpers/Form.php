<?php

namespace App\Helpers;

class Form
{

    // Form
    public static function open($name, $action, $method, $params = null)
    {
        return view("components.form", compact("name", "action", "method", "params"));
    }

    public static function close($disabled = false)
    {
        $html = "";
        // if ($disabled) {
        //     $html .= "</fieldset>";
        // }
        $html .= "</form>";
        echo $html;
    }


    // Input
    public static function input($type, $name, $value, $params = null)
    {
        return view("components.input", compact("type", "name", "value", "params"));
    }

    // Texteare
    public static function textarea($name, $value, $params = null)
    {
        return view("components.textarea", compact("name", "value", "params"));
    }

    //select
    public static function select($name, $options, $value, $params = null)
    {
        return view("components.select", compact("name", "options", "value", "params"));
    }
}
