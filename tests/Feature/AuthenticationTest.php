<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\URL;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationTest extends TestCase
{
    /** @test */
    public function cantAccessHomeWithoutLogin()
    {
        $response = $this->get(route("home"));

        $response->assertStatus(302)
            ->assertLocation(route("login"));
    }

    /** @test */
    public function canAccessLoginPageWithoutLogin()
    {
        $response = $this->get(route("login"));

        $response->assertStatus(200)
            ->assertSee("Entrar");
    }

    /** @test */
    public function loginValidationRulesAreOk()
    {
        $response = $this->post(route("login.auth"), [
            "user"=> "",
            "password" => "32131"
        ]);

        $response->assertRedirect();
    }
}
