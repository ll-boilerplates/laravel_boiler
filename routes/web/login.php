<?php


Route::middleware("guest")->prefix("/login")->group(function () {
    Route::get("/", "LoginController@showLoginForm")->name("login");
    Route::post("/", "LoginController@login")->name("login.auth");
});

Route::middleware("auth")->prefix("logout")->group(function () {
    Route::get("/", "LoginController@logout")->name("logout");
});

Route::get("/impersonate/{credential}", "LoginController@impersonate")
    ->name("login.impersonate")
    ->middleware("auth");
