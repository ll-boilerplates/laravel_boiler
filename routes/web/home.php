<?php

use App\Notifications\ResetPasswordEmail;

// Route::middleware("auth")->group(function () {
    Route::redirect('/', '/home');
    Route::get("/home", "HomeController@index")->name("home");
// });


Route::get('/mail', function () {
    $user = App\Models\User::find(1);

    return (new ResetPasswordEmail("321321321321", $user))
                ->toMail($user);
});
