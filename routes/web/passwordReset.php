<?php


Route::middleware("guest")->name("password.")->prefix("password")->group(function() {
    Route::get("/redefinir/solicitar", "ForgotPasswordController@showLinkRequestForm")->name("form");
    Route::post("/redefinir/enviar", "ForgotPasswordController@sendResetLinkEmail")->name("send");

    Route::get("/redefinir/{token}", "ResetPasswordController@showResetForm")->name("reset.form");
    Route::post("/redefinir", "ResetPasswordController@reset")->name("reset");
});
