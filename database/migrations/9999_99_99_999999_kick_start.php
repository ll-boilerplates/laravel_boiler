<?php

use App\Models\Role;
use App\Models\User;
use App\Models\RoleUser;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\Models\PermissionGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KickStart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $admin = User::create([
            "name"=>"Administador",
            "email"=>"luiz.lahr@w2z.com.br",
            "username"=>"admin",
            "password"=>Hash::make("321321"),
            "active" => true
        ]);

        $admin->id = 0;
        $admin->save();

        DB::select("delete from sqlite_sequence where name='users'");

        $user = User::create([
            "name"=>"Luiz Lahr",
            "email"=>"boivl@hotmail.com",
            "username"=>"luiz",
            "password"=>Hash::make("321321"),
            "active" => true
        ]);

        $role = Role::create([
            "name"=>"Administrador",
            "description"=>"Perfil com todos os acessos do sistema"
        ]);

        $role->id = 0;
        $role->save();

        $group = PermissionGroup::create([
            "name"=>"Autenticação"
        ]);

        $permission = Permission::create([
            "name"=>"Acessar o Sistema",
            "permission"=> "auth_login",
            "permission_group_id"=> $group->id
        ]);

        PermissionRole::create([
            "permission_id"=> $permission->id,
            "role_id"=> $role->id
        ]);

        RoleUser::create([
            "role_id"=> $role->id,
            "user_id"=> $admin->id
        ], [
            "role_id"=> $role->id,
            "user_id"=> $user->id
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
