@extends('layout.base')


@section('body')

    <main>
        @yield('content')
    </main>

@endsection
