<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ env("APP_NAME") . " - " . env("APP_VERSION") }}</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    @stack("styles")

</head>
<body class="@yield('bodyClass')">

    @yield("body")

    @stack("modals")
    @stack("scripts")

</body>
</html>
