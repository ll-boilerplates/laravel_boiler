@extends('layout.main')

@section("bodyClass", "login-page")

@section("content")
    <form action="{{ route("login.auth") }}" method="POST">
        @csrf

        @input(["params" => ["text", "user", null, ["required", "autofocus", "placeholder"=>"Usuário / E-mail"]]])@endinput
        @input(["params" => ["password", "password", null, ["required", "placeholder"=>"Senha"]]])@endinput

        <input type="submit" value="Entrar">
        <a href="{{ route("password.form") }}">Esqueci a senha :(</a>
    </form>
@endsection
