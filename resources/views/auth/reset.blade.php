@extends('layout.main')

@section("bodyClass", "login-page")

@section("content")
<form action="{{ route("password.reset") }}" method="POST">
    @csrf
    <input type="hidden" name="token" value={{ $token }}>

    @input(["params" => ["email", "email", null, ["required", "autofocus", "placeholder"=>"Informe seu e-mail"]]])@endinput
    @input(["params" => ["password", "password", null, ["required", "placeholder"=>"Informe sua senha"]]])@endinput
    @input(["params" => ["password", "password_confirmation", null, ["required", "placeholder"=>"Confirme sua senha"]]])@endinput

    <input type="submit" value="Redefinir Senha">
</form>
@endsection
