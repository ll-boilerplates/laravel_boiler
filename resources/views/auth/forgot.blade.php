@extends('layout.main')

@section("bodyClass", "login-page")

@section("content")
    <form action="{{ route("password.send") }}" method="POST">
        @csrf
        @input(["params" => ["email", "email", null, ["required", "autofocus", "placeholder"=>"Informe seu e-mail"]]])@endinput
        <input type="submit" value="Enviar Lembrete">
        <a href="{{ route("login") }}">Voltar</a>
    </form>
@endsection
