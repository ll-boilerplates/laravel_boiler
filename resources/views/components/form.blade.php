@php
$method = in_array(strtolower($method), ["get", "post"]) ? $method : "post";
$disabled = false;
@endphp

<form name={{ $name }} action="{{ $action }}" method="{{ $method }}" @if(!empty($params)) @if(is_array($params))
    @foreach($params as $attribute=> $param)
    @if($attribute != "disabled")
    @if(!is_int($attribute))
    {!! $attribute . "='" . $param ."'" !!}
    @else
    {!! $param !!}
    @endif
    @else
    @php
    $disabled = true;
    @endphp
    @endif
    @endforeach
    @else
    {{ $params }}
    @endif
    @endif
    >
    @csrf

    @if(!in_array(strtolower($method), ["get", "post"]))
    @method($method)
    @endif

    @if($disabled)<fieldset class="form-disabled" disabled>@endif
