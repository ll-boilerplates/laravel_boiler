@php
    $name = !empty($params[0]) ? $params[0] : Str::random(10);;
    $value = !empty($params[1]) ? $params[1] : old($name);
@endphp
<textarea
    name="{{ $name }}"
    @if(isset($params[2]) && !empty($params[2]))
        @if(is_array($params[2]))
            @foreach($params[2] as $attribute => $param)
                @if(!is_int($attribute))
                    {!! $attribute . "='" . $param ."'" !!}
                @else
                    {!! $param !!}
                @endif
            @endforeach
        @else
        {{ $params[2] }}
        @endif
    @endif
>{{$value}}</textarea>
@if ($errors->has($name))
    @php
        $message = $errors->first($name);
    @endphp
    <div class="input-error-msg alert alert-danger">{{ $message }}</div>
@endif
