@php
    $name = !empty($params[0]) ? $params[0] : Str::randon(10);
    $options = !empty($params[1]) ? $params[1] : [];
    $value = !empty($params[2]) ? $params[2] : old($name);
@endphp

<select
    name="{{ $name }}"

    @if(isset($params[3]) && !empty($params[3]))
        @if(is_array($params[3]))
            @foreach($params[3] as $attribute => $param)
                @if(!$attribute == "placeholder")
                    @if(!is_int($attribute))
                        {!! $attribute . "='" . $param ."'" !!}
                    @else
                        {!! $param !!}
                    @endif
                @endif
            @endforeach
        @else
        {{ $params[3] }}
        @endif
    @endif
>
    @if(isset($params[3]["placeholder"]))
    <option value="">{!! $params[3]["placeholder"] !!}</option>
    @endif


    @if(count($options) > 0)
        @foreach($options as $index => $option)
            <option value="{{ $index }}" {{ $value != $index ?null: "selected" }}>{{ $option }}</option>
        @endforeach
    @endif

</select>
@if ($errors->has($name))
    @php
        $message = $errors->first($name);
    @endphp
    <div class="input-error-msg alert alert-danger">{{ $message }}</div>
@endif
