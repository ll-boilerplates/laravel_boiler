<?php
?>
@php
$value = $value ?? old($name);
@endphp

<input type="{{ $type }}" name="{{ $name }}" value="{{ $value }}" @if(isset($params) && !empty($params))
    @if(is_array($params)) @foreach($params as $attribute=>
$param)
@if(!is_int($attribute)) {!! $attribute . "='" . $param ."'" !!} @else {!!
$param !!} @endif @endforeach @else
{{ $params }}
@endif @endif /> @if ($errors->has($name)) @php $message =
$errors->first($name); @endphp
<div class="input-error-msg alert alert-danger">{{ $message }}</div>
@endif
