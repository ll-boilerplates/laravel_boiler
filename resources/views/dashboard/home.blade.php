@extends('layout.base')

@section("body")
<div>
    @php
    $options = [
    1=>"teste 1",
    2=>"teste 2"
    ];
    @endphp

    {{ Form::open("frmTest", "route('home')", "post", ["disabled"]) }}
    {{ Form::input("text", "txtName", "Luiz Lahr", ["id"=>"name", "style"=>"background-color: #000;"]) }}
    {{ Form::close() }}
</div>
@endsection
