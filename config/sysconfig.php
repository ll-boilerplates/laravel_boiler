<?php

return [


    /**
     *
     * AUTH SETTINGS
     * setting the auth parameters
     *
     */
    "auth" => [

        /**
         *
         * Allows logged users with permission (need configuration)
         * to log in as another user,by providing username, email or ID
         *
         * This feature is implemented to assist support crew verify reported
         * errors
         *
         * suggested permission: impersonate
         *
         */
        "impersonation" => true,


        /**
         *
         * Allows users to log in with username instead of only email
         * Both require password
         *
         */
        "usernameLogin" => true,


        /**
         *
         * Defines if users will be automatcly remembered or not
         *
         */
        "alwaysRemeber" => true,
    ],

];
